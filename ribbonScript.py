

"""	
						RIBBON SCRIPT 

						FLEXIPLANE !!!!!
"""

import maya.cmds as cmds
class MyRibbonFlexiPlane:

	def ribbonFlexiPlane(self,obj,baseName,ratio,detail,radius):

		rad=radius
		cmds.select(clear=True)
		ribbonSystem_grp=cmds.createNode('transform',n=baseName+"_ribbonSystem_grp")
		cmds.select(clear=True)
		
		posA=cmds.xform(obj[0], t=True ,ws=True ,query=True)
		posB=cmds.xform(obj[1], t=True ,ws=True ,query=True)

		CurvA=cmds.curve(d=1 ,n=baseName+"_curvA",p=[(posA[0],posA[1],posA[2]), (posB[0],posB[1],posB[2])] )
		Shape=cmds.listRelatives(CurvA, shapes=True)
		CurveAShape=cmds.rename(Shape,str(CurvA)+"Shape")
		CurveAinfo=cmds.arclen(baseName+'_curvA',n=baseName+"_curvAInfo",ch=True)
		distanceCurveA=cmds.getAttr(CurveAinfo+".arcLength")

		#find the middle and its orient
		middCurvLoc=cmds.spaceLocator(a=True, n=baseName+"_middCurv_loc")
		middCurv4x4=cmds.createNode('fourByFourMatrix',name=baseName+"_middCurv_4x4")
		middDecompMatrix=cmds.createNode('decomposeMatrix', name=baseName+"_middPointMatrix")
		cmds.connectAttr((middCurv4x4+'.output'),(middDecompMatrix+'.inputMatrix'))
		cmds.connectAttr((middDecompMatrix+'.outputTranslate'),(middCurvLoc[0]+'.translate'))
		cmds.connectAttr((middDecompMatrix+'.outputRotate'),(middCurvLoc[0]+'.rotate'))



			#Translate:
		sommeTrans=cmds.createNode('plusMinusAverage', name=baseName+"_sumPoints")
		MD_translate=cmds.createNode('multiplyDivide',name=baseName+"_dividePoints")

		cmds.setAttr(MD_translate+'.operation',2)
		cmds.connectAttr((CurveAShape + '.controlPoints[0]'), (sommeTrans + '.input3D[0]'))
		cmds.connectAttr((CurveAShape + '.controlPoints[1]'), (sommeTrans + '.input3D[1]'))
		cmds.connectAttr((sommeTrans + '.output3D'), (MD_translate + '.input1'))
		cmds.setAttr(MD_translate+'.input2.input2X',2)
		cmds.setAttr(MD_translate+'.input2.input2Y',2)
		cmds.setAttr(MD_translate+'.input2.input2Z',2)
		cmds.connectAttr((MD_translate + '.output.outputX'),(middCurv4x4 + '.in30'))
		cmds.connectAttr((MD_translate + '.output.outputY'),(middCurv4x4 + '.in31'))
		cmds.connectAttr((MD_translate + '.output.outputZ'),(middCurv4x4 + '.in32'))

			#orient
		soustractionTrans=cmds.createNode('plusMinusAverage', name=baseName+"_subPoints")
		middPointVP_x=cmds.createNode('vectorProduct', name=baseName+"_middPointOrient_x")
		middPointVP_y=cmds.createNode('vectorProduct', name=baseName+"_middPointOrient_y")
		middPointVP_z=cmds.createNode('vectorProduct', name=baseName+"_middPointOrient_z")


		cmds.connectAttr((CurveAShape + '.controlPoints[0]'), (soustractionTrans + '.input3D[1]'))
		cmds.connectAttr((CurveAShape + '.controlPoints[1]'), (soustractionTrans + '.input3D[0]'))
		cmds.setAttr(soustractionTrans+'.operation',2)
		cmds.connectAttr((soustractionTrans + '.output3D'), (middPointVP_x + '.input1'))
		cmds.connectAttr((soustractionTrans + '.output3D'), (middPointVP_z + '.input1'))
		cmds.connectAttr((middPointVP_x + '.output'), (middPointVP_y + '.input1'))
		cmds.connectAttr((soustractionTrans + '.output3D.output3Dx'), (middCurv4x4 + '.in00'))
		cmds.connectAttr((soustractionTrans + '.output3D.output3Dy'), (middCurv4x4 + '.in01'))
		cmds.connectAttr((soustractionTrans + '.output3D.output3Dz'), (middCurv4x4 + '.in02'))
		cmds.connectAttr((middPointVP_y + '.output.outputX'), (middCurv4x4 + '.in10'))
		cmds.connectAttr((middPointVP_y + '.output.outputY'), (middCurv4x4 + '.in11'))
		cmds.connectAttr((middPointVP_y + '.output.outputZ'), (middCurv4x4 + '.in12'))
		cmds.connectAttr((middPointVP_z + '.output.outputX'), (middCurv4x4 + '.in20'))
		cmds.connectAttr((middPointVP_z + '.output.outputY'), (middCurv4x4 + '.in21'))
		cmds.connectAttr((middPointVP_z + '.output.outputZ'), (middCurv4x4 + '.in22'))
		cmds.setAttr(middPointVP_x +'.operation',2)
		cmds.setAttr(middPointVP_y +'.operation',2)
		cmds.setAttr(middPointVP_z +'.operation',2)
		cmds.setAttr(middPointVP_x +'.input2.input2Y',1)
		cmds.setAttr(middPointVP_y +'.input2.input2X',1)
		cmds.setAttr(middPointVP_z +'.input2.input2Z',1)

		posM=cmds.xform(middCurvLoc, t=True ,ws=True ,query=True)
		rotM=cmds.xform(middCurvLoc, ro=True ,ws=True ,query=True)








		#create surface nurbs
		CurvA_01=cmds.curve(d=1 ,n=baseName + "_curve_A_01",p=[(0,0,-1), (0,0,1)] )
		cmds.xform(CurvA_01,scale=[1,1,1])
		cmds.FreezeTransformations()
		cmds.DeleteHistory()
		cmds.xform(CurvA_01,ws=True,t=[posA[0],posA[1],posA[2]])
		cmds.xform(CurvA_01,ws=True,ro=[rotM[0],rotM[1],rotM[2]])
		ShapeA=cmds.listRelatives(CurvA_01, shapes=True)
		ShapeA_01=cmds.rename(ShapeA,CurvA_01+"Shape")

		CurvA_02=cmds.duplicate(CurvA_01)
		cmds.xform(CurvA_02,ws=True,t=[posB[0],posB[1],posB[2]])
		cmds.xform(CurvA_02,ws=True,ro=[rotM[0],rotM[1],rotM[2]])
		ShapeA_02=cmds.listRelatives(CurvA_02, shapes=True)



		if detail >= 1:
			ribbonPlaneLow=cmds.loft(ShapeA_01, ShapeA_02,ch=False ,name=baseName+"_ribbonPlane_01", rn=True, ar=True, ss=ratio-1)
			print(ribbonPlaneLow)
			cmds.select(clear=True)
			
		else:
			print"detail attribut need to be 1,2 or 3"

		if detail >= 2:
			ribbonPlaneMid=cmds.loft(ShapeA_01, ShapeA_02,ch=False,name=baseName+"_ribbonPlane_02", rn=True, ar=True, ss=(ratio*2)-2)
			print(ribbonPlaneMid)
			cmds.select(clear=True)
			
		else:
			print"detail attribut need to be 1,2 or 3"

		if detail == 3:
			ribbonPlaneHd=cmds.loft(ShapeA_01, ShapeA_02,ch=False ,name=baseName+"_ribbonPlane_03", rn=True, ar=True, ss=((ratio*2-1)*2)-2)
			print(ribbonPlaneHd)
			cmds.select(clear=True)
		else:
			print"detail attribut need to be 1,2 or 3"











		#Create controllers
		ribbCtrlsGroup=cmds.createNode('transform',n=baseName+"_ribbon_Ctrls_grp")
		#create Circle



		if detail >=1:
			ribbonCtrlsMainList=[]
			ribbonJointsMainList=[]
			ribbonOffsetMainList=[]

			for i in range(0,2):
				ribbonCtrlsMainList.append(cmds.circle(normal=(1,0,0),n=baseName+"_ribbCtrlMain_0"+str(1+i),ch=False,r=rad))
				ribbonJointsMainList.append(cmds.joint(n=baseName+"_ribbJointMain_0"+str(1+i),r=15))
				ribbonOffsetMainList.append(cmds.createNode('transform',n=baseName+"_ribbCtrlMainOffset_0"+str(1+i)))
				cmds.parent(ribbonCtrlsMainList[i],ribbonOffsetMainList[i])
				cmds.select(clear=True)
				cmds.parent(ribbonOffsetMainList[i],ribbCtrlsGroup)

			cmds.xform(ribbonOffsetMainList[0],ws=True,t=(posA[0],posA[1],posA[2]))
			cmds.xform(ribbonOffsetMainList[0],ws=True,ro=(rotM[0],rotM[1],rotM[2]))
			cmds.xform(ribbonOffsetMainList[1],ws=True,t=(posB[0],posB[1],posB[2]))
			cmds.xform(ribbonOffsetMainList[1],ws=True,ro=(rotM[0],rotM[1],rotM[2]))



			ribbonCtrlsLowList=[]
			ribbonJointsLowList=[]
			ribbonOffsetLowList=[]

			for i in range(0,ratio):
				ribbonCtrlsLowList.append(cmds.circle(normal=(1,0,0),n=baseName+"_ribbCtrlLow_0"+str(1+i),ch=False,r=rad-1))
				ribbonJointsLowList.append(cmds.joint(n=baseName+"_ribbJointLow_0"+str(1+i),r=15))
				ribbonOffsetLowList.append(cmds.createNode('transform',n=baseName+"_ribbCtrlLowOffset_0"+str(1+i)))
				cmds.parent(ribbonCtrlsLowList[i],ribbonOffsetLowList[i])
				cmds.select(clear=True)
				cmds.parent(ribbonOffsetLowList[i],ribbCtrlsGroup)

		else:
			print"detail attribut need to be 1,2 or 3"

		if detail >=2 :
			ribbonCtrlsMidList=[]
			ribbonJointsMidList=[]
			ribbonOffsetMidList=[]

			for i in range(0,(ratio*2-1)):
				ribbonCtrlsMidList.append(cmds.circle(normal=(1,0,0),n=baseName+"_ribbCtrlMid_0"+str(1+i),ch=False,r=rad-1.5))
				ribbonJointsMidList.append(cmds.joint(n=baseName+"_ribbJointMid_0"+str(1+i),r=1))
				ribbonOffsetMidList.append(cmds.createNode('transform',n=baseName+"_ribbCtrlMidOffset_0"+str(1+i)))
				cmds.parent(ribbonCtrlsMidList[i],ribbonOffsetMidList[i])
				cmds.select(clear=True)
				cmds.parent(ribbonOffsetMidList[i],ribbCtrlsGroup)
		else:
			print"detail attribut need to be 1,2 or 3"

		if detail ==3 :
			ribbonCtrlsHdList=[]
			ribbonJointsHdList=[]
			ribbonOffsetHdList=[]

			for i in range(0,((ratio*2-1)*2-1)):
				ribbonCtrlsHdList.append(cmds.circle(normal=(1,0,0),n=baseName+"_ribbCtrlHd_0"+str(1+i),ch=False,r=rad-2))
				ribbonJointsHdList.append(cmds.joint(n=baseName+"_ribbJointHd_0"+str(1+i),r=5))
				ribbonOffsetHdList.append(cmds.createNode('transform',n=baseName+"_ribbCtrlHdOffset_0"+str(1+i)))
				cmds.parent(ribbonCtrlsHdList[i],ribbonOffsetHdList[i])
				cmds.select(clear=True)
				cmds.parent(ribbonOffsetHdList[i],ribbCtrlsGroup)
		else:
			print"detail attribut need to be 1,2 or 3"










		#creer les follicules:
		ribbFollGroup=cmds.createNode('transform',n=baseName+"_ribbon_follicule_grp")
		#folliculeLow
		if detail >=1:
			ribbonPlaneLowShape=baseName+"_ribbonPlane_01Shape"
			rivetTransLow=[]
			rivetLow=[]

			for i in range (0,ratio):
				rivetLow.append(cmds.createNode('follicle',n=baseName+"_ribbFollLow_0"+str(i+1)))
				rivetTransLow.append(cmds.listRelatives(rivetLow[i], type='transform', p=True))
				cmds.connectAttr(rivetLow[i]+'.outRotate', ((rivetTransLow[i])[0])+'.rotate')
				cmds.connectAttr(rivetLow[i]+'.outTranslate', ((rivetTransLow[i])[0])+'.translate')
				cmds.setAttr(rivetLow[i]+'.simulationMethod',0)
				cmds.connectAttr(ribbonPlaneLowShape+".local",rivetLow[i]+".inputSurface")
				cmds.connectAttr(ribbonPlaneLowShape+".worldMatrix",rivetLow[i]+".inputWorldMatrix")
				u=((distanceCurveA/(ratio-1))/distanceCurveA)*(i)
				v=0.5
				cmds.setAttr(rivetLow[i]+'.parameterU',u)
				cmds.setAttr(rivetLow[i]+'.parameterV',v)
				cmds.parentConstraint(((rivetTransLow[i])[0]), ribbonOffsetLowList[i], mo=False)
				cmds.parent(((rivetTransLow[i])[0]), ribbFollGroup)
				cmds.select(clear=True)
		else:
			print"detail attribut need to be 1,2 or 3"


		#folliculeMid
		if detail >=2 :
			ribbonPlaneMidShape=baseName+"_ribbonPlane_02Shape"
			rivetTransMid=[]
			rivetMid=[]


			for i in range (0,(ratio*2-1)):
				rivetMid.append(cmds.createNode('follicle',n=baseName+"_ribbFollMid_0"+str(i+1)))
				rivetTransMid.append(cmds.listRelatives(rivetMid[i], type='transform', p=True))
				cmds.connectAttr(rivetMid[i]+'.outRotate', ((rivetTransMid[i])[0])+'.rotate')
				cmds.connectAttr(rivetMid[i]+'.outTranslate', ((rivetTransMid[i])[0])+'.translate')
				cmds.setAttr(rivetMid[i]+'.simulationMethod',0)
				cmds.connectAttr(ribbonPlaneMidShape+".local",rivetMid[i]+".inputSurface")
				cmds.connectAttr(ribbonPlaneMidShape+".worldMatrix",rivetMid[i]+".inputWorldMatrix")
				u=((distanceCurveA/(ratio*2-2))/distanceCurveA)*(i)
				v=0.5
				cmds.setAttr(rivetMid[i]+'.parameterU',u)
				cmds.setAttr(rivetMid[i]+'.parameterV',v)
				cmds.parentConstraint(((rivetTransMid[i])[0]), ribbonOffsetMidList[i], mo=False)
				cmds.parent(((rivetTransMid[i])[0]), ribbFollGroup)
				cmds.select(clear=True)

		else:
			print"detail attribut need to be 1,2 or 3"


		#folliculeHd
		if detail ==3 :
			ribbonPlaneHdShape=baseName+"_ribbonPlane_03Shape"
			rivetTransHd=[]
			rivetHd=[]


			for i in range (0,((ratio*2-1)*2-1)):
				rivetHd.append(cmds.createNode('follicle',n=baseName+"_ribbFollHd_0"+str(i+1)))
				rivetTransHd.append(cmds.listRelatives(rivetHd[i], type='transform', p=True))
				cmds.connectAttr(rivetHd[i]+'.outRotate', ((rivetTransHd[i])[0])+'.rotate')
				cmds.connectAttr(rivetHd[i]+'.outTranslate', ((rivetTransHd[i])[0])+'.translate')
				cmds.setAttr(rivetHd[i]+'.simulationMethod',0)
				cmds.connectAttr(ribbonPlaneHdShape+".local",rivetHd[i]+".inputSurface")
				cmds.connectAttr(ribbonPlaneHdShape+".worldMatrix",rivetHd[i]+".inputWorldMatrix")
				u=((distanceCurveA/((ratio*2-1)*2-2))/distanceCurveA)*(i)
				v = 0.5
				cmds.setAttr(rivetHd[i]+'.parameterU',u)
				cmds.setAttr(rivetHd[i]+'.parameterV',v)
				cmds.parentConstraint(((rivetTransHd[i])[0]), ribbonOffsetHdList[i], mo=False)
				cmds.parent(((rivetTransHd[i])[0]), ribbFollGroup)
				cmds.select(clear=True)
		else:
			print"detail attribut need to be 1,2 or 3"


		#skinning les planes
		if detail >=1 :
			cmds.skinCluster( ribbonJointsMainList, ribbonPlaneLow, dr=4.5, n=baseName+"_lowPlane_skinCluster",tsb=True)
			cmds.select(clear=True)
			for i in range(0,ratio+2):
	   			cmds.select(ribbonPlaneLow[0]+'.cv[%i]' % i,ribbonPlaneLow[0]+'.cv[%i]' % (i+1))
				cmds.skinPercent(baseName+"_lowPlane_skinCluster",transformValue=[(ribbonJointsMainList[0], 1-(i*1/(ratio+2))),(ribbonJointsMainList[1], (i*1/(ratio+2)))],normalize=False )
				cmds.skinPercent( baseName+"_lowPlane_skinCluster", ribbonPlaneLow[0]+'.cv[%i]' % (i), normalize=True )

		else:
			print"detail attribut need to be 1,2 or 3"

		if detail >=2 :
			cmds.skinCluster( ribbonJointsLowList, ribbonPlaneMid, dr=4.5,n=baseName+"_midPlane_skinCluster",tsb=True)
			for i in range(0,(ratio*2)/2):
				cmds.select(ribbonPlaneMid[0] + '.cv[%i]' % i,ribbonPlaneMid[0] +'.cv[%i]' % (i+1))
				cmds.skinPercent( baseName+"_midPlane_skinCluster",transformValue=[(str(ribbonJointsLowList[0]), 1-(i*1/(ratio*2))),(str(ribbonJointsLowList[1]), (i*1/(ratio*2)))],normalize=False )
				# cmds.select(ribbonPlaneMid[0]+'.cv['+str((ratio*2)/2+i)+']' [0] ,ribbonPlaneMid[0]+'.cv['+str((ratio*2)/2+i)+']'+'['+str(1)+']')
				# cmds.skinPercent( baseName+"_midPlane_skinCluster",transformValue=[(str(ribbonJointsLowList[1]), 1-(i*1/(ratio*2))),(str(ribbonJointsLowList[2]), (i*1/(ratio*2)))],normalize=False )
				# cmds.skinPercent( baseName+"_midPlane_skinCluster", ribbonPlaneMid[0]+'.cv[%i]' % (i), normalize=True )
				# cmds.skinPercent( baseName+"_midPlane_skinCluster", ribbonPlaneMid[0]+'.cv[%i]' % (i+1), normalize=True )

		else:
			print"detail attribut need to be 1,2 or 3"

		if detail ==3 :
			cmds.skinCluster( ribbonJointsMidList, ribbonPlaneHd, dr=4.5,n=baseName+"_highPlane_skinCluster",tsb=True)
			# for i in range(0,(ratio*2-1)*2):
			# 	cmds.select(ShapeA_03+'.cv[%i]' % i,ShapeA_03+'.cv[%i]' % (i+1))
			# 	cmds.skinPercent( baseName+"_highPlane_skinCluster", transformValue=[(ribbonJointsMidList[0], 1-(i*1/((ratio*2-1)*2)),(ribbonJointsMidList[1], (i*1/((ratio*2-1)*2)))] )


		else:
			print"detail attribut need to be 1,2 or 3"





		#parent
		cmds.parent(ribbCtrlsGroup,ribbonSystem_grp)
		cmds.select(clear=True)
		cmds.parent(ribbFollGroup,ribbonSystem_grp)
		cmds.select(clear=True)
		cmds.parent(ribbonPlaneLow,ribbonSystem_grp)
		cmds.select(clear=True)
		cmds.parent(ribbonPlaneMid,ribbonSystem_grp)
		cmds.select(clear=True)
		cmds.parent(ribbonPlaneHd,ribbonSystem_grp)
		cmds.select(clear=True)

		#deleteallNoUseNode
		cmds.delete(baseName + "_middCurv_loc")
		cmds.delete(baseName + "_curve_A_01")
		cmds.delete(baseName + "_curve_A_02")
		cmds.delete(baseName + "_curvA")




