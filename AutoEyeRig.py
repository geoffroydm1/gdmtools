#SCRIPT EYELID

import maya.cmds as cmds


class eyelidScript:

	def CubeCtrl(self,EyeGlassSelect):
		bbox=[]
		for i in EyeGlassSelect:
			bbox.append(cmds.exactWorldBoundingBox(i))
		size=1
		for elem in bbox:
			s = max(elem[3]-elem[0], elem[4]-elem[1], elem[5]-elem[2])
			if s > size:
				size = s
		size = size/2
		return [(-size,size,size),(size,size,size),(size,size,-size),(-size,size,-size),(-size,size,size),(-size,-size,size),(size,-size,size),(size,size,size),(size,-size,size),(size,-size,-size),(size,size,-size),(size,-size,-size),(-size,-size,-size),(-size,size,-size),(-size,-size,-size),(-size,-size,size)]

	def eye(self,EyeBallSelect,EyeGlassSelect,side):

		#basename:
		objectName=str(EyeGlassSelect[0])
		listObjectName=list(objectName.split('_',3))
		listObjectName.remove(listObjectName[-1])
		basename='_'.join(listObjectName)

		eyePos=cmds.xform(EyeGlassSelect[0],ws=True,sp=True,query=True)
		eyeRot=cmds.xform(EyeGlassSelect[0],ws=True,ro=True,query=True)

		eyeSystemGrp=cmds.createNode("transform",n=basename+"_eyeSystem_"+side[0]+"_grp")
		eyeCtrlsGrp=cmds.createNode("transform",n=basename+"_eyeCtrls_"+side[0]+"_grp")
		eyejointsGrp=cmds.createNode("transform",n=basename+"_eyeJoints_"+side[0]+"_grp")


		#Creation JOINTS
		cmds.select(clear=True)
		eyeJointCenter=cmds.joint(n=basename+"_eye_"+side[0]+"_joint_00")
		eyeJoint01=cmds.joint(n=basename+"_eye_"+side[0]+"_joint_01")
		eyeJoint02=cmds.joint(n=basename+"_eye_"+side[0]+"_joint_02")
		cmds.xform(eyeJointCenter,t=(eyePos[0],eyePos[1],eyePos[2]),ro=(eyeRot[0],eyeRot[1],eyeRot[2]))
		cmds.xform(eyeJoint02,ws=False,t=(0,0,1))
		cmds.select(clear=True)

		#creation controllers eye:
		shapeSize=self.CubeCtrl(EyeBallSelect)[1][0]
		fkEyeCtrl=cmds.curve(d=1, n=basename+"_eye_"+side[0]+"_fk_ctrl", p=([0.0,3,0.0],[-1,2,1],[-0.5,2,0.5],[-0.5,1,0.5],[0.5,1,-0.5],[0.5,2,-0.5],[1,2,-1],[0.0,3,0.0],[1,2,1],[0.5,2,0.5],[0.5,1,0.5],[-0.5,1,-0.5],[-0.5,2,-0.5],[-1,2,-1],[0.0,3.00,0.0]))
		fkEyeCtrlCvs=cmds.select(str(fkEyeCtrl)+".cv[0]",str(fkEyeCtrl)+".cv[1]",str(fkEyeCtrl)+".cv[2]",str(fkEyeCtrl)+".cv[3]",str(fkEyeCtrl)+".cv[4]",str(fkEyeCtrl)+".cv[5]",str(fkEyeCtrl)+".cv[6]",str(fkEyeCtrl)+".cv[7]",str(fkEyeCtrl)+".cv[8]",str(fkEyeCtrl)+".cv[9]",str(fkEyeCtrl)+".cv[10]",str(fkEyeCtrl)+".cv[11]",str(fkEyeCtrl)+".cv[12]",str(fkEyeCtrl)+".cv[13]",str(fkEyeCtrl)+".cv[14]")
		cmds.xform(ws=True,ro=[90,0,45],s=[0.5*shapeSize,0.5*shapeSize,0.5*shapeSize])
		fkEyeCtrlShape=cmds.listRelatives(fkEyeCtrl,shapes=True)
		fkEyeCtrlShape=cmds.rename(str(fkEyeCtrlShape[0]),str(fkEyeCtrl)+"Shape")
		cmds.select(cl=True)

		eyeCtrlAimcst=cmds.createNode("transform",n=str(fkEyeCtrl)+"_cst")
		EyeGrpFocus=cmds.createNode("transform",n=str(fkEyeCtrl)+"_focus_grp")
		fkEyeCtrlOffset=cmds.createNode("transform",n=str(fkEyeCtrl)+"_offset")
		cmds.select(clear=True)
		cmds.parent(fkEyeCtrl,eyeCtrlAimcst)
		cmds.select(clear=True)
		cmds.parent(eyeCtrlAimcst,EyeGrpFocus)
		cmds.select(clear=True)
		cmds.parent(EyeGrpFocus,fkEyeCtrlOffset)
		cmds.select(clear=True)

		if side == "Right":
			cmds.xform(fkEyeCtrlOffset, ws=True, ro=[0,0,180], t=[eyePos[0],eyePos[1],eyePos[2]])

		if side == "Left":
			cmds.xform(fkEyeCtrlOffset, ws=True, ro=[0,0,0], t=[eyePos[0],eyePos[1],eyePos[2]])


		cmds.addAttr(fkEyeCtrl,longName="iris",attributeType='float',minValue=int(-2), maxValue=int(2),keyable=1)
		cmds.select(clear=True)
		eyeFkCstOr=cmds.orientConstraint(fkEyeCtrl, eyeJoint01, mo=False)

		#Creation mainCtrl
		eyemainCtrl=cmds.curve(d=1,n=basename+"_eye_"+side[0]+"_main_ctrl",p=self.CubeCtrl(EyeBallSelect))
		eyemainCtrlShape=cmds.listRelatives(eyemainCtrl,shapes=True)
		eyemainCtrlShape=cmds.rename(str(eyemainCtrlShape[0]),str(eyemainCtrl)+"Shape")
		cmds.select(cl=True)

		Main_grp=cmds.group(eyemainCtrl)
		eyemainCtrlOffset=cmds.rename(Main_grp,str(eyemainCtrl)+"_offset")

		if side == "Right":
			cmds.xform(eyemainCtrlOffset, ws=True, t=(eyePos[0],eyePos[1],eyePos[2]))

		if side == "Left":
			cmds.xform(eyemainCtrlOffset, ws=True, t=(eyePos[0],eyePos[1],eyePos[2]))

		cmds.select(clear=True)
		cmds.parentConstraint(eyemainCtrl,eyeJointCenter,mo=False)

		#Creation AimCtrl
		eyeAimCtrl=cmds.circle(nr=(1, 0, 0), n=basename + "_eye_" + side[0] + "_Aim_Ctrl", sw=360, radius=1, sections=8, ch=False)
		cmds.select (str(eyeAimCtrl[0]) + '.cv[0]', str(eyeAimCtrl[0]) + '.cv[2]', str(eyeAimCtrl[0]) + '.cv[4]', str(eyeAimCtrl[0]) + '.cv[6]', r=True, add=True)
		cmds.xform(ws=True, t=[0,0,0])
		cmds.select(clear=True)
		cmds.xform(eyeAimCtrl, ws=True, ro=[0,90,0])
		cmds.makeIdentity(eyeAimCtrl, apply=True, t=1, r=1, s=1, n=0, pn=1)
		cmds.select(clear=True)
		eyeAimCtrlOffset = cmds.group(str(eyeAimCtrl[0]), n=str(eyeAimCtrl[0])+"_offset")
		eyeAimCtrlShape = cmds.listRelatives(eyeAimCtrl, shapes=True)
		cmds.select(cl=True)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".rotateX", lock=True, k=False, channelBox=False)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".rotateY", lock=True, k=False, channelBox=False)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".rotateZ", lock=True, k=False, channelBox=False)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".scaleX", lock=True, k=False, channelBox=False)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".scaleY", lock=True, k=False, channelBox=False)
		cmds.setAttr(str(eyeAimCtrl[0]) + ".scaleZ", lock=True, k=False, channelBox=False)

		if side == "Right":
			cmds.xform(eyeAimCtrlOffset, ws=True, ro=[0,-180,0], t=(eyePos[0], eyePos[1], 25))

		if side == "Left":
			cmds.xform(eyeAimCtrlOffset, ws=True, t=(eyePos[0], eyePos[1], 25))

		#Do AIM constaint
		cmds.addAttr(eyeAimCtrl, longName="AimSwitch", attributeType='float', minValue=int(0), maxValue=int(1), keyable=1)
		eyeAimcst=cmds.aimConstraint( eyeAimCtrl, eyeCtrlAimcst, mo=True, name=basename + "_eye_" + side[0] + "_AimCst", worldUpObject=eyemainCtrl, worldUpType="objectrotation") 
		cmds.connectAttr(str(eyeAimCtrl[0]) + ".AimSwitch", str(eyeAimcst[0]) + "." + eyeAimCtrl[0] + "W0")

		#Do focus
		cmds.addAttr(eyeAimCtrl, longName="Focus", attributeType='float', minValue=int(-90), maxValue=int(90), keyable=1)
		if side == "Left":
			cmds.connectAttr(str(eyeAimCtrl[0]) + ".Focus", str(EyeGrpFocus) + ".rotateY")

		if side == "Right":
			eyeFocusREV=cmds.createNode("reverse",n=basename + "_eye_" + side[0] + "_focus_REV")
			cmds.connectAttr(str(eyeAimCtrl[0]) + ".Focus", str(eyeFocusREV) + ".input.inputX")
			cmds.connectAttr(str(eyeFocusREV) + ".output.outputX", str(EyeGrpFocus) + ".rotateY")


		#do scale Connect
		eyeScaleSUM=cmds.createNode("plusMinusAverage", n=basename + "_eye_" + side[0] + "_SUM")
		cmds.connectAttr(str(fkEyeCtrl) + ".iris", str(eyeScaleSUM) + ".input1D[0]")
		cmds.connectAttr(str(eyemainCtrl) + ".scaleX", str(eyeScaleSUM) + ".input1D[1]")
		cmds.connectAttr(str(eyeScaleSUM) + ".output1D", str(eyeJoint02) + ".scaleX")
		cmds.connectAttr(str(eyeScaleSUM) + ".output1D", str(eyeJoint02) + ".scaleY")

		cmds.connectAttr(str(eyemainCtrl) + ".scaleX", str(eyeJointCenter) + ".scaleX")
		cmds.connectAttr(str(eyemainCtrl) + ".scaleY", str(eyeJointCenter) + ".scaleY")
		cmds.connectAttr(str(eyemainCtrl) + ".scaleZ", str(eyeJointCenter) + ".scaleZ")

		cmds.connectAttr(str(eyemainCtrl) + ".scaleX", str(eyeJoint01) + ".scaleX")
		cmds.connectAttr(str(eyemainCtrl) + ".scaleY", str(eyeJoint01) + ".scaleY")
		cmds.connectAttr(str(eyemainCtrl) + ".scaleZ", str(eyeJoint01) + ".scaleZ")

		cmds.connectAttr(str(eyemainCtrl) + ".scaleZ", str(eyeJoint02) + ".scaleZ")

		#setColors
		if side == "Right":
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideEnabled", 1)
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideRGBColors", 0)
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideColor", 13)

			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideEnabled", 1)
			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideRGBColors", 0)
			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideColor", 13)

			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideEnabled", 1)
			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideRGBColors", 0)
			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideColor", 13)


		if side == "Left":
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideEnabled", 1)
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideRGBColors", 0)
			cmds.setAttr(str(eyemainCtrlShape) + ".overrideColor", 6)

			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideEnabled", 1)
			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideRGBColors", 0)
			cmds.setAttr(str(fkEyeCtrlShape) + ".overrideColor", 6)

			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideEnabled", 1)
			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideRGBColors", 0)
			cmds.setAttr(str((eyeAimCtrlShape)[0]) + ".overrideColor", 6)

		#Clean Outliner
		cmds.parent(fkEyeCtrlOffset,eyemainCtrl)
		cmds.select(clear=True)
		cmds.parent(eyeJointCenter,eyejointsGrp)
		cmds.select(clear=True)
		cmds.parent(eyejointsGrp,eyeSystemGrp)
		cmds.select(clear=True)
		cmds.parent(eyeAimCtrlOffset,eyeCtrlsGrp)
		cmds.select(clear=True)
		cmds.parent(eyemainCtrlOffset,eyeCtrlsGrp)
		cmds.select(clear=True)
		cmds.parent(eyeCtrlsGrp,eyeSystemGrp)
		cmds.select(clear=True)

		#do skin
		for i in range (0,len(EyeBallSelect)):
			cmds.skinCluster(eyeJoint01, eyeJoint02, EyeBallSelect[i], n= basename + "_eyeBall_" + side[0] + "_skincluster_" + "%02d" % (i+1,), tsb=True)
		cmds.select(clear=True)

		cmds.skinCluster(eyeJoint01, EyeGlassSelect[0], n= basename + "_eyeGlass_" + side[0] + "_skincluster_01", tsb=True)
		cmds.select(cl=True)

		#Follow attr
		 
		cmds.addAttr(fkEyeCtrl, longName="follow", attributeType='bool', h=False,k=True)
		cmds.select(cl=True)
		followgroup=cmds.createNode("transform",n=str(fkEyeCtrl)+"_follow_cst")
		cmds.xform(followgroup,ws=True, t=[eyePos[0],eyePos[1],eyePos[2]])
		cmds.select(cl=True)
		cmds.parent(followgroup,eyemainCtrl)
		cmds.select(cl=True)
		cmds.parent(eyemainCtrlOffset,followgroup)
		cmds.select(cl=True)
		followOrientCst=cmds.orientConstraint(fkEyeCtrl,followgroup,mo=True)
		followOrientMD=cmds.createNode("multiplyDivide",n=str(fkEyeCtrl)+"_follow_MD")
		cmds.setAttr(str(followOrientMD)+".operation",2)
		cmds.setAttr(str(followOrientMD)+".input2.input2X",4)
		cmds.connectAttr(str(fkEyeCtrl)+".follow",str(followOrientMD)+".input1.input1X")
		cmds.connectAttr(str(followOrientMD)+".output.outputX",str(followOrientCst)+".fkEyeCtrlW0")




	def eyelid(self,vertexList,eyelid,side,master):

		#create BaseName
		masterName=str(master)
		print(master)
		listMasterName=list(masterName.split('_',3))
		print(listMasterName)
		listMasterName.remove(listMasterName[-1])
		print(listMasterName)
		basename='_'.join(listMasterName)
		print(basename)

		#determine side color
		if side == "Right":
			colorCtrl=13

		if side == "Left":
			colorCtrl=6

		#create hierarchy
		eyelidGrp=cmds.createNode("transform", n= basename + "_eyelid_" + side[0] + "_" + eyelid + "_grp")
		eyelidSystemGrp=cmds.createNode("transform", n= basename + "_eyelidSystem_" + side[0] + "_" + eyelid + "_grp")
		eyelidCtrlsGrp=cmds.createNode("transform", n= basename + "_eyelidCtrls_" + side[0] + "_" + eyelid + "_grp")
		eyelidjointsGrp=cmds.createNode("transform", n= basename + "_eyelidJoints_" + side[0] + "_" + eyelid + "_grp")
		cmds.select(cl=True)
		cmds.parent(eyelidSystemGrp, eyelidGrp)
		cmds.select(cl=True)
		cmds.parent(eyelidCtrlsGrp, eyelidGrp)
		cmds.select(cl=True)
		cmds.parent(eyelidjointsGrp, eyelidGrp)
		cmds.select(cl=True)

		#eyebase joint creation
		eyePos=cmds.xform(master, ws=True, t=True, query=True)
		eyeRot=cmds.xform(master, ws=True, ro=True, query=True)
		cmds.select(clear=True)

		eyeCenterJoint=cmds.joint(n=basename + "_eye_" + side[0] + "_" + eyelid + "_baseJoint", p= (eyePos[0], eyePos[1], eyePos[2]), r=0.25)
		cmds.parent(eyeCenterJoint, eyelidjointsGrp)
		cmds.select(clear=True)



		#get vertex positions
		# print("vertexList:")
		# for i in vertexList:
		# 	print(i)

		vertexPos=[]
		for i in vertexList:
			vertexPos.append(cmds.xform(i, ws=True, t=True, query=True))

		# print("vertexPosition:")
		# for i in vertexPos:
		# 	print(i)

		#eyelid joints creations
		eyelidJoints=[]
		eyelidBaseJoints=[]

		for i in range(0,len(vertexList)):
			eyelidJoints.append(cmds.joint(n=basename + "_eyelid_" + side[0] + "_" + eyelid + "_joint_" + "%02d" % (i+1,), p=[(vertexPos[i])[0],(vertexPos[i])[1],(vertexPos[i])[2]]))
			cmds.select(clear=True)

		for i in range(0,len(vertexList)):
			eyelidBaseJoints.append(cmds.joint(n=basename + "_eyelid_" + side[0] + "_" + eyelid + "_baseJoint_" + "%02d" % (i+1,), p=[eyePos[0],eyePos[1],eyePos[2]]))
			cmds.parent(eyelidBaseJoints[i], eyeCenterJoint)
			cmds.select(clear=True)
			cmds.parent(eyelidJoints[i], eyelidBaseJoints[i])
			cmds.select(eyelidBaseJoints[i])
			cmds.joint(e=True, oj='xyz', secondaryAxisOrient='yup', ch=True, zso=True)
			cmds.select(clear=True)
			cmds.setAttr(str(eyelidJoints[i]) + ".jointOrientX",0)
			cmds.setAttr(str(eyelidJoints[i]) + ".jointOrientY",0)
			cmds.setAttr(str(eyelidJoints[i]) + ".jointOrientZ",0)
			cmds.select(clear=True)

		for i in eyelidJoints:
			cmds.connectAttr(str(master) + ".scaleX", str(i) + ".scaleX")
			cmds.connectAttr(str(master) + ".scaleY", str(i) + ".scaleY")
			cmds.connectAttr(str(master) + ".scaleZ", str(i) + ".scaleZ")

		for i in eyelidBaseJoints:
			cmds.connectAttr(str(master) + ".scaleX", str(i) + ".scaleX")
			cmds.connectAttr(str(master) + ".scaleY", str(i) + ".scaleY")
			cmds.connectAttr(str(master) + ".scaleZ", str(i) + ".scaleZ")

		#Set label joints
		if side=="Right":
			for i in eyelidJoints:
				cmds.setAttr(str(i) + ".side", 2)

		if side=="Left":
			for i in eyelidJoints:
				cmds.setAttr(str(i) + ".side", 1)

		#get uValue of vertex selectionned
		from math import pow,sqrt

		distList=[]
		for i in range(0,len(vertexList)-1):			
			distList.append( sqrt( pow( (vertexPos[i+1])[0]-(vertexPos[i])[0],2 ) + pow( (vertexPos[i+1])[1]-(vertexPos[i])[1],2 ) + pow( (vertexPos[i+1])[2] - (vertexPos[i])[2],2) ) *10)
			# print("distList:"+str(distList[i]))		


		vertexUValueList = list(distList)
		# print("vertexUValueList:")
		# for i in vertexUValueList:
		# 	print(i)

		for i in range(0, len(distList)):
		    for j in range(0, i):
		        vertexUValueList[i] += distList[j]


		curvDist=sum(distList)

		# print("curvDist:")
		# print(curvDist)

			



		#curve creation for motion path
		#create linear curv with position of the vextex selected
		
		#Low Curv
		eyelidLow=cmds.curve(d=3,n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_low_curv",p=((0,0,0),(0,0,1),(0,0,2),(0,0,3),(0,0,4)))
		cmds.xform(str(eyelidLow)+".cv[0]",ws=True,t=[(vertexPos[0])[0],(vertexPos[0])[1],(vertexPos[0])[2]])
		cmds.xform(str(eyelidLow)+".cv[4]",ws=True,t=[(vertexPos[len(vertexList)-1])[0],(vertexPos[len(vertexList)-1])[1],(vertexPos[len(vertexList)-1])[2]])
		for i in range(0,3):
			cmds.xform(str(eyelidLow)+".cv["+str(i+1)+"]",ws=True,t=[(vertexPos[int((i+1)*len(vertexList)/5)])[0],(vertexPos[(i+1)*int(len(vertexList)/5)])[1],(vertexPos[int((i+1)*len(vertexList)/5)])[2]])
		eyelidLowShape=cmds.listRelatives(eyelidLow,shapes=1)
		eyelidLowShape=cmds.rename(eyelidLowShape[0],basename+"_eyelid_"+side[0]+"_"+eyelid+"_low_curvShape")

		#hd Curv
		eyelidHdLinear=cmds.curve(d=1,n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_hd_curv",p=((0,0,0),(0,0,2)))
		eyelidHdLinear=cmds.rebuildCurve( eyelidHdLinear , constructionHistory=0, degree=1, spans=len(vertexList)-1, rebuildType=0, replaceOriginal=1) 
		for i in range(0,len(vertexList)):
			cmds.xform(str(eyelidHdLinear[0])+".cv["+str(i)+"]",ws=True,t=[(vertexPos[i])[0],(vertexPos[i])[1],(vertexPos[i])[2]])
		eyelidHd=cmds.rebuildCurve( eyelidHdLinear , constructionHistory=0, degree=3, fitRebuild=1, spans=6, rebuildType=0, replaceOriginal=1) 
		cmds.delete(str(eyelidHd[0])+".cv[1]",str(eyelidHd[0])+".cv[7]")
		eyelidHdShape=cmds.listRelatives(eyelidHd,shapes=1)
		eyelidHdShape=cmds.rename(eyelidHdShape[0],basename+"_eyelid_"+side[0]+"_"+eyelid+"_hd_curvShape")


		#clean hierarchy Curv
		cmds.parent(eyelidLow,eyelidSystemGrp)
		cmds.select(clear=1)
		cmds.parent(eyelidHd,eyelidSystemGrp)
		cmds.select(clear=1)


		#create locators for motion path
		#hd locators
		hdLocators=[]
		hdLocPathCst=[]
		for i in range(0,len(vertexList)):
			hdLocators.append(cmds.spaceLocator(n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_hd_loc_"+ "%02d" % (i+1,)))
			cmds.select(clear=True)
			hdLocPathCst.append(cmds.pathAnimation( hdLocators[i], curve=str(eyelidHd[0]), fractionMode=True, name=basename+"_eyelid_"+side[0]+"_"+eyelid+"_pathCst_hd_"+ "%02d" % (i+1,), worldUpObject=str(master), worldUpType="objectrotation"))
			cmds.delete(str(hdLocPathCst[i])+"_uValue")
			cmds.select(clear=True)

		# for i in range(0,len(vertexUValueList)):
		# 	print(vertexUValueList[i])

		for i in range(0,len(hdLocators)-1):
			cmds.setAttr(str(hdLocPathCst[i+1])+".uValue",float(((vertexUValueList[i])/curvDist)))
			
		for i in range(0,len(hdLocators)):
			cmds.parent(hdLocators[i],eyelidCtrlsGrp)



		#low locators
		lowLocators=[]
		lowLocPathCst=[]
		for i in range(0,3):
			lowLocators.append(cmds.spaceLocator(n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_low_loc_"+ "%02d" % (i+1,)))
			cmds.select(clear=True)
			lowLocPathCst.append(cmds.pathAnimation( lowLocators[i] , curve=str(eyelidLow), fractionMode=True, name=basename+"_eyelid_"+side[0]+"_"+eyelid+"_pathCst_low_"+ "%02d" % (i+1,), worldUpObject=str(master), worldUpType="objectrotation"))
			cmds.delete(str(lowLocPathCst[i])+"_uValue")
			cmds.select(clear=True)

		cmds.setAttr(str(lowLocPathCst[0])+".uValue",float(((vertexUValueList[int(len(vertexList)/4)])/curvDist)))
		cmds.setAttr(str(lowLocPathCst[1])+".uValue",float(((vertexUValueList[int(len(vertexList)/4)*2])/curvDist)))
		cmds.setAttr(str(lowLocPathCst[2])+".uValue",float(((vertexUValueList[int(len(vertexList)/4)*3])/curvDist)))

		for i in range(0,len(lowLocators)):
			cmds.parent(lowLocators[i],eyelidCtrlsGrp)



		#MAIN CTRL

		mainCtrl=cmds.curve(d=1, n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_blink_ctrl_01",p=[(0,0.42,0.86),(0.26,0.57,0.80),(0.5,0.7,0.7),(0.25,0.70,0.70),(0.25,0.92,0.38),(0.25,1,0),(0.25,0.92,-0.38),(0.25,0.70,-0.70),(0.25,0.38,-0.92),(0.25,0,-1),(0.25,-0.38,-0.92),(0.5,-0.38,-0.92),(0.25,-0.50,-0.85),(0,-0.62,-0.73),(-0.25,-0.50,-0.85),(-0.5,-0.38,-0.92),(-0.25,-0.38,-0.92),(-0.25,0,-1),(-0.25,0.38,-0.92),(-0.25,0.70,-0.70),(-0.25,0.92,-0.38),(-0.25,1,0),(-0.25,0.92,0.38),(-0.25,0.70,0.70),(-0.5,0.70,0.70),(-0.26,0.57,0.80),(0,0.42,0.86)])
		cmds.xform(mainCtrl,ro=[-65,0,0],s=[2.0,3.0,3.0])
		cmds.FreezeTransformations(mainCtrl)
		mainCtrlShape=cmds.listRelatives(mainCtrl,shapes=True)
		mainCtrlShape=cmds.rename(mainCtrlShape[0],basename+"_eyelid_"+side[0]+"_"+eyelid+"_blink_ctrl_01Shape")
		cmds.setAttr(str(mainCtrlShape) + ".overrideEnabled",1)
		cmds.setAttr(str(mainCtrlShape) + ".overrideRGBColors",0)
		cmds.setAttr(str(mainCtrlShape) + ".overrideColor",colorCtrl)
		cmds.select(clear=True)

		mainCtrlOffset=cmds.createNode('transform',n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_blink_ctrl_01_offset")
		cmds.select(cl=True)
		cmds.parent(mainCtrl,mainCtrlOffset)
		cmds.select(cl=True)
		mainJoint = cmds.joint(n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_jt_main_01")
		cmds.parent(mainJoint,mainCtrl)
		cmds.select(cl=True)

		if eyelid == "Up":
			cmds.xform(mainCtrlOffset,ro=[eyeRot[0]+90,eyeRot[1]-180,eyeRot[2]])

		if eyelid == "Down":
			cmds.xform(mainCtrlOffset,ro=[-60,0,0])

		cmds.parent(mainCtrlOffset, master)
		cmds.select(cl=True)
		cmds.xform(mainCtrlOffset,t=[0,0,0])
		cmds.select(cl=True)

		cmds.setAttr(str(mainCtrl)+".tx",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".tx",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".ty",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".ty",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".tz",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".tz",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".ry",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".ry",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".rz",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".rz",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".sx",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".sx",channelBox=False)
		cmds.setAttr(str(mainCtrl)+".sy",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".sy",channelBox=False)		
		cmds.setAttr(str(mainCtrl)+".sz",lock=True, keyable=False)
		cmds.setAttr(str(mainCtrl)+".sz",channelBox=False)

		cmds.select(mainCtrl)
		cmds.addAttr(shortName="EYELID",longName="EYELID", attributeType='bool',h=False,k=True)
		cmds.addAttr(shortName="Ctrl_Low",longName="Ctrl_Low", attributeType='bool',h=False,k=True)
		cmds.addAttr(shortName="Ctrl_HD",longName="Ctrl_HD", attributeType='bool',h=False,k=True)
		cmds.setAttr(str(mainCtrl)+".EYELID",lock=True)




		#lowCtrls
		LowS=0.2

		lowCtrlList=[]
		lowCtrlOffsetList=[]
		for i in range(0,3):
			lowCtrlList.append(cmds.curve(d=1, n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_low_ctrl_"+ "%02d" % (i+1,),p=[(-LowS,LowS,LowS),(LowS,LowS,LowS),(LowS,LowS,-LowS),(-LowS,LowS,-LowS),(-LowS,LowS,LowS),(-LowS,-LowS,LowS),(LowS,-LowS,LowS),(LowS,LowS,LowS),(LowS,-LowS,LowS),(LowS,-LowS,-LowS),(LowS,LowS,-LowS),(LowS,-LowS,-LowS),(-LowS,-LowS,-LowS),(-LowS,LowS,-LowS),(-LowS,-LowS,-LowS),(-LowS,-LowS,LowS)]))
			lowCtrlOffsetList.append(cmds.createNode("transform",n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_low_ctrl_"+ "%02d" % (i+1,)+"_offset"))
			cmds.select(clear=True)
			cmds.parent(lowCtrlList[i],lowCtrlOffsetList[i])
			cmds.xform(lowCtrlOffsetList[i],ws=True,ro=[90,0,0],s=[1,1,1])
			cmds.select(clear=True)
			lowCtrlShape=cmds.listRelatives(lowCtrlList[i],shapes=True)
			lowCtrlShape=cmds.rename(str(lowCtrlShape[0]),str(lowCtrlList[i])+"Shape")
			cmds.setAttr(str(lowCtrlShape) + ".overrideEnabled",1)
			cmds.setAttr(str(lowCtrlShape) + ".overrideRGBColors",0)
			cmds.setAttr(str(lowCtrlShape) + ".overrideColor",colorCtrl)
			cmds.xform(lowCtrlShape,ws=True,t=[0,1,0])
			cmds.select(clear=True)


		#creation of HDCtrls
		HdS=0.1

		hdCtrlList=[]
		hdCtrlOffsetList=[]
		for i in range(0,len(vertexList)):
			hdCtrlList.append(cmds.curve(d=1,n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_hd_ctrl_"+ "%02d" % (i+1,),p=[(-HdS,HdS,HdS),(HdS,HdS,HdS),(HdS,HdS,-HdS),(-HdS,HdS,-HdS),(-HdS,HdS,HdS),(-HdS,-HdS,HdS),(HdS,-HdS,HdS),(HdS,HdS,HdS),(HdS,-HdS,HdS),(HdS,-HdS,-HdS),(HdS,HdS,-HdS),(HdS,-HdS,-HdS),(-HdS,-HdS,-HdS),(-HdS,HdS,-HdS),(-HdS,-HdS,-HdS),(-HdS,-HdS,HdS)]))
			hdCtrlOffsetList.append(cmds.createNode("transform",n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_hd_ctrl_0"+ "%02d" % (i+1,)+"_offset"))
			cmds.select(clear=True)
			cmds.parent(hdCtrlList[i],hdCtrlOffsetList[i])
			cmds.select(clear=True)
			hdCtrlShape=cmds.listRelatives(hdCtrlList[i],shapes=True)
			hdCtrlShape=cmds.rename(str(hdCtrlShape[0]),str(hdCtrlList[i])+"Shape")
			cmds.setAttr(str(hdCtrlShape) + ".overrideEnabled",1)
			cmds.setAttr(str(hdCtrlShape) + ".overrideRGBColors",0)
			cmds.setAttr(str(hdCtrlShape) + ".overrideColor",colorCtrl)
			cmds.select(clear=True)
			cmds.parent(hdCtrlOffsetList[i],hdLocators[i])
			cmds.select(clear=True)
			cmds.xform(hdCtrlOffsetList[i],t=[0,0,0])
			cmds.select(clear=True)

		lowJointLow=[]
		for i in range(0,3):
			lowJointLow.append(cmds.joint(n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_jt_low_"+str(i+1)))
			cmds.parent(lowJointLow[i],lowCtrlList[i])
			cmds.xform(lowJointLow[i],t=[0,0,0])
			cmds.parent(lowCtrlOffsetList[i],lowLocators[i])
			cmds.select(clear=True)
			cmds.xform(lowCtrlOffsetList[i],t=[0,0,0])
			cmds.select(clear=True)


		#creation of aimCst 
		aimCstList=[]
		for i in range(0,len(eyelidBaseJoints)):
			aimCstList.append(cmds.aimConstraint(hdCtrlList[i],eyelidBaseJoints[i],n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_aimCst_"+ "%02d" % (i+1,),mo=False, worldUpObject=master, worldUpType="objectrotation"))
			cmds.select(clear=True)

		#skin curves
		cmds.skinCluster( mainJoint, eyeCenterJoint , str(eyelidLow)+"Shape", dr=4.5,n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster",toSelectedBones=True,ignoreHierarchy=True,normalizeWeights=True,weightDistribution=0)
		cmds.skinCluster(basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster",e=True,mi=6)
		cmds.skinPercent(basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+"Shape", pruneWeights=0.24, normalize=True )
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+".cv[0]", transformValue=[(mainJoint, 0), (eyeCenterJoint, 1)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+".cv[1]", transformValue=[(mainJoint, 0.75), (eyeCenterJoint, 0.25)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+".cv[2]", transformValue=[(mainJoint, 1), (eyeCenterJoint, 0)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+".cv[3]", transformValue=[(mainJoint, 0.75), (eyeCenterJoint, 0.25)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_lowCurv_skinCluster", str(eyelidLow)+".cv[4]", transformValue=[(mainJoint, 0), (eyeCenterJoint, 1)])
		cmds.select(clear=True)

		hdCurvCvsNbr= cmds.getAttr(str(eyelidHd[0])+".cp",s=1)

		cmds.skinCluster( lowJointLow, eyeCenterJoint, str(eyelidHd[0])+"Shape", dr=4.5,n=basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster",toSelectedBones=True,ignoreHierarchy=True,normalizeWeights=True,weightDistribution=0)
		cmds.skinCluster(basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster",e=True,mi=6)
		cmds.skinPercent(basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster", str(eyelidHd[0])+"Shape", pruneWeights=0.24, normalize=True )
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster", str(eyelidHd[0])+".cv[0]", transformValue=[(lowJointLow[0], 0), (eyeCenterJoint, 1)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster", str(eyelidHd[0])+".cv[1]", transformValue=[(lowJointLow[0], 0.5), (eyeCenterJoint, 0.5)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster", str(eyelidHd[0])+".cv["+str(int(hdCurvCvsNbr-1))+"]", transformValue=[(lowJointLow[2], 0.5), (eyeCenterJoint, 0.5)])
		cmds.skinPercent( basename+"_eyelid_"+side[0]+"_"+eyelid+"_hdCurv_skinCluster", str(eyelidHd[0])+".cv["+str(int(hdCurvCvsNbr))+"]", transformValue=[(lowJointLow[2], 0), (eyeCenterJoint, 1)])
		cmds.select(clear=True)


		#SCALE + Point Constraint
		cmds.parentConstraint(master,eyeCenterJoint,mo=True)
		cmds.scaleConstraint(master,eyeCenterJoint,mo=True)

		#connectAttr ctrls visibility

		for i in lowCtrlList:
			cmds.connectAttr(str(mainCtrl)+".Ctrl_Low",str(i)+".visibility")

		for i in hdCtrlList:
			cmds.connectAttr(str(mainCtrl)+".Ctrl_HD",str(i)+".visibility")



