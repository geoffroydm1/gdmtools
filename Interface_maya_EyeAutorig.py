try:
  from PySide2.QtCore import * 
  from PySide2.QtGui import * 
  from PySide2.QtWidgets import *
  from PySide2 import __version__
  from shiboken2 import wrapInstance 
except ImportError:
  from PySide.QtCore import * 
  from PySide.QtGui import * 
  from PySide import __version__
  from shiboken import wrapInstance 

import sys, os.path
import maya.cmds as cmds
from maya import OpenMayaUI as omui 

path = os.path.abspath(os.path.dirname(__file__))
if not path in sys.path:
    sys.path.append(path)

import AutoEyeRig
reload(AutoEyeRig)


# Recupere la fenetre principale de maya
mayaMainWindowPtr = omui.MQtUtil.mainWindow()
mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget) 

AER= AutoEyeRig.eyelidScript()

#creation de la fenetre
class EyeRigUI(QWidget):
	def __init__(self):
		super(EyeRigUI,self).__init__()

		#lien entre la fenetre maya et notre fenetre
		self.setParent(mayaMainWindow)
		self.setWindowFlags(Qt.Window)

		#details de notre fenetre
		self.setObjectName("EyeRigUI")
		self.setWindowTitle("EyeRigUI")
		self.setGeometry(1500,50,360,300)
		self.initUI()
		self.show()

		self.EyeGlassList = []
		self.EyeBallList= []
		self.vertexSelected = []
		self.masterSelected = []

	def initUI(self):
		#architecture Eye
		self.EyeLabel = QLabel(self)
		self.EyeLabel.setText("EYE RIG")
		self.EyeLabel.move(20,20)

		self.EyeGlassLabel = QLabel(self)
		self.EyeGlassLabel.setText("EyeGlass:")
		self.EyeGlassLabel.move(20,40)

		self.EyeGlassButton = QPushButton("Select",self)
		self.EyeGlassButton.move(150,35)
		self.EyeGlassButton.clicked.connect(self.EyeGlassFunction)

		self.EyeBallLabel = QLabel(self)
		self.EyeBallLabel.setText("EyeBall:")
		self.EyeBallLabel.move(20,70)

		self.EyeBallButton = QPushButton("Select",self)
		self.EyeBallButton.move(150,65)
		self.EyeBallButton.clicked.connect(self.EyeBallFunction)

		self.EyeRigSideLabel = QLabel(self)
		self.EyeRigSideLabel.setText("Side:")
		self.EyeRigSideLabel.move(210,40)

		self.EyeRigSide = QComboBox(self)
		self.EyeRigSide.addItem("Left")
		self.EyeRigSide.addItem("Right")
		self.EyeRigSide.setCurrentIndex(0)
		self.EyeRigSide.move(235,38)

		self.EyeRunButton = QPushButton("Run",self)
		self.EyeRunButton.move(20,100)
		self.EyeRunButton.clicked.connect(self.EyeRun)



		#architecture Eyelid
		self.EyelidLabel = QLabel(self)
		self.EyelidLabel.setText("EYELID RIG")
		self.EyelidLabel.move(20,140)

		self.VertexSelectLabel = QLabel("Selected Vertex Order:",self)
		self.VertexSelectLabel.move(20,160)

		self.VertexSelectButton = QPushButton("Select",self)
		self.VertexSelectButton.move(150,155)
		self.VertexSelectButton.clicked.connect(self.vertexEyelidFunction)

		self.EyelidRigSideLabel = QLabel(self)
		self.EyelidRigSideLabel.setText("Side:")
		self.EyelidRigSideLabel.move(210,160)

		self.EyelidRigSide = QComboBox(self)
		self.EyelidRigSide.addItem("Left")
		self.EyelidRigSide.addItem("Right")
		self.EyelidRigSide.setCurrentIndex(0)
		self.EyelidRigSide.move(240,160)

		self.EyelidRigUpDwnLabel = QLabel(self)
		self.EyelidRigUpDwnLabel.setText("Eyelid:")
		self.EyelidRigUpDwnLabel.move(210,190)

		self.EyelidRigUpDwn = QComboBox(self)
		self.EyelidRigUpDwn.addItem("Up")
		self.EyelidRigUpDwn.addItem("Down")
		self.EyelidRigUpDwn.setCurrentIndex(0)
		self.EyelidRigUpDwn.move(245,190)

		self.EyelidMasterLabel = QLabel(self)
		self.EyelidMasterLabel.setText("Master:")
		self.EyelidMasterLabel.move(20,190)

		self.EyelidMasterButton = QPushButton("Select",self)
		self.EyelidMasterButton.move(150,185)
		self.EyelidMasterButton.clicked.connect(self.EyelidMaster)

		self.EyelidRunButton = QPushButton("Run",self)
		self.EyelidRunButton.move(20,220)
		self.EyelidRunButton.clicked.connect(self.EyelidRun)


	def EyeGlassFunction(self):
		self.EyeGlassList = cmds.ls(sl=True)
		print(self.EyeGlassList)

	def EyeBallFunction(self):
		self.EyeBallList= cmds.ls(sl=True)
		print(self.EyeBallList)

	def EyeRun(self): 
		AER.eye(EyeBallSelect=self.EyeBallList, EyeGlassSelect=self.EyeGlassList, side=self.EyeRigSide.currentText())



	def vertexEyelidFunction(self):
		self.vertexSelected = cmds.ls(os=True)
		print(self.vertexSelected)

	def EyelidMaster(self):
		self.masterSelected = cmds.ls(sl=True)
		print(self.masterSelected)

	def EyelidRun(self):
		AER.eyelid(vertexList=self.vertexSelected,side=self.EyelidRigSide.currentText(),eyelid=self.EyelidRigUpDwn.currentText(),master=self.masterSelected[0])



