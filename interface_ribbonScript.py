try:
  from PySide2.QtCore import * 
  from PySide2.QtGui import * 
  from PySide2.QtWidgets import *
  from PySide2 import __version__
  from shiboken2 import wrapInstance 
except ImportError:
  from PySide.QtCore import * 
  from PySide.QtGui import * 
  from PySide import __version__
  from shiboken import wrapInstance 

import sys, os.path
import maya.cmds as cmds
from maya import OpenMayaUI as omui 

path = os.path.abspath(os.path.dirname(__file__))
if not path in sys.path:
    sys.path.append(path)

import ribbonScript
reload(ribbonScript)


# Recupere la fenetre principale de maya
mayaMainWindowPtr = omui.MQtUtil.mainWindow()
mayaMainWindow = wrapInstance(long(mayaMainWindowPtr), QWidget) 

ribb= ribbonScript.MyRibbonFlexiPlane()

#creation de la fenetre
class ribbonScriptUI(QWidget):
	def __init__(self):
		super(ribbonScriptUI,self).__init__()

		#lien entre la fenetre maya et notre fenetre
		self.setParent(mayaMainWindow)
		self.setWindowFlags(Qt.Window)

		#details de notre fenetre
		self.setObjectName("ribbonScriptUI")
		self.setWindowTitle("ribbonScriptUI")
		self.setGeometry(1500,50,360,300)
		self.initUI()
		self.show()

		self.objectList = []
		self.detail = []

	def initUI(self):
		#architecture Eye
		self.RibbonLabel = QLabel(self)
		self.RibbonLabel.setText("FLEXI PLANE")
		self.RibbonLabel.move(20,20)

		self.objectLabel = QLabel(self)
		self.objectLabel.setText("object:")
		self.objectLabel.move(20,40)

		self.objectSelectButton = QPushButton("Select",self)
		self.objectSelectButton.move(60,35)
		self.objectSelectButton.clicked.connect(self.objectSelectFunction)

		self.ratioLabel = QLabel(self)
		self.ratioLabel.setText("ratio:")
		self.ratioLabel.move(210,70)

		self.ratioSpiner =  QSpinBox(self)
		self.ratioSpiner.setRange(0, 10)
		self.ratioSpiner.setValue(3)
		self.ratioSpiner.setDisplayIntegerBase(10)
		self.ratioSpiner.move(250,70)

		self.basenameLabel = QLabel(self)
		self.basenameLabel.setText("name:")
		self.basenameLabel.move(20,70)

		self.basenameLineEdit = QLineEdit(self)
		self.basenameLineEdit.setMaxLength(20)
		self.basenameLineEdit.setText("ch_test_x01x")
		self.basenameLineEdit.move(60,65)
		# self.basenameLineEdit.maxLength()

		self.DetailLabel = QLabel(self)
		self.DetailLabel.setText("Detail:")
		self.DetailLabel.move(210,40)

		self.DetailComboBox = QComboBox(self)
		self.DetailComboBox.addItem("1")
		self.DetailComboBox.addItem("2")
		self.DetailComboBox.addItem("3")		
		self.DetailComboBox.setCurrentIndex(2)
		self.DetailComboBox.move(255,38)

		self.radiusLabel = QLabel(self)
		self.radiusLabel.move(20,100)
		self.radiusLabel.setText("jtRadius:")

		self.radiusSpiner =  QSpinBox(self)
		self.radiusSpiner.setRange(0, 10)
		self.radiusSpiner.setDisplayIntegerBase(10)
		self.radiusSpiner.move(80,95)

		self.RibbRunButton = QPushButton("Run",self)
		self.RibbRunButton.move(20,140)
		self.RibbRunButton.clicked.connect(self.RibbRun)



	def objectSelectFunction(self):
		self.objectList = cmds.ls(sl=True)
		print(self.objectList)

	def RibbRun(self):
		print(self.basenameLineEdit.text())
		print(self.ratioSpiner.value())
		print(self.DetailComboBox.currentText())
		print(self.radiusSpiner.value())
		print(self.objectList)

		ribb.ribbonFlexiPlane(obj=self.objectList, baseName=self.basenameLineEdit.text(), ratio=self.ratioSpiner.value(), detail=int(self.DetailComboBox.currentText()), radius=self.radiusSpiner.value())

